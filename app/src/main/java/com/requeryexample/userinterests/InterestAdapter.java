package com.requeryexample.userinterests;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.requeryexample.R;
import com.requeryexample.models.User;
import com.requeryexample.models.UserInterest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fedor on 06.12.2016.
 */

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.DeviceHolder> {

    private Context mContext;

    private List<UserInterest> mList = new ArrayList<>();


    public InterestAdapter(Context context) {
        this.mContext = context;
    }


    // 3
    public class DeviceHolder extends RecyclerView.ViewHolder {

        TextView deviceName;

        public DeviceHolder(View itemView) {
            super(itemView);
            deviceName = (TextView) itemView.findViewById(R.id.item_user_text);

        }

        public void update(final UserInterest item) {
            deviceName.setText("" + item.getInterest());
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(final DeviceHolder holder, final int position) {
        final UserInterest place = mList.get(position);
        holder.update(place);
    }

    public void update(List<UserInterest> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public UserInterest getItem(int postion) {
        return mList.get(postion);
    }
}
