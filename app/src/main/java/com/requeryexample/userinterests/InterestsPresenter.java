package com.requeryexample.userinterests;

import com.requeryexample.models.User;
import com.requeryexample.models.UserInterest;
import com.requeryexample.userslist.UsersView;

import java.util.List;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class InterestsPresenter {

    private final InterestsView mUsersView;
    //Todo implement Dagger injection
    private ReactiveEntityStore<Persistable> mData;

    public InterestsPresenter(ReactiveEntityStore<Persistable> data, InterestsView view) {
        mUsersView = view;
        mData = data;
    }

    public void getInterests() {
        mUsersView.showProgress();

        List<UserInterest> usersList = mData.select(UserInterest.class).get().toList();

        mUsersView.onInterestsQueryCompleted(usersList);

        mUsersView.hideProgress();
    }

}
