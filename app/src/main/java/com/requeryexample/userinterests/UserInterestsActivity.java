package com.requeryexample.userinterests;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.requeryexample.App;
import com.requeryexample.BaseActivity;
import com.requeryexample.R;
import com.requeryexample.models.UserInterest;

import java.util.List;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class UserInterestsActivity extends BaseActivity implements InterestsView {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private InterestAdapter mAdapter;
    private InterestsPresenter interestsPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        renderView();

        ReactiveEntityStore<Persistable> data = ((App) getApplication()).getData();
        interestsPresenter = new InterestsPresenter(data, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        interestsPresenter.getInterests();
    }

    @Override
    public void renderView() {
        super.renderView();
        setContentView(R.layout.activity_interests);
        mRecyclerView = (RecyclerView) findViewById(R.id.interests_list);
        mProgress = (ProgressBar) findViewById(R.id.interests_progress);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onInterestsQueryCompleted(List<UserInterest> list) {
        mAdapter = new InterestAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.update(list);
    }
}
