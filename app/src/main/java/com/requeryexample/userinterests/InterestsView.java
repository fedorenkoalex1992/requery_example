package com.requeryexample.userinterests;

import com.requeryexample.models.UserInterest;

import java.util.List;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public interface InterestsView {

    void showProgress();

    void hideProgress();

    void onInterestsQueryCompleted(List<UserInterest> list);
}
