package com.requeryexample.models;

import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToOne;
import io.requery.Persistable;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

@Entity
public interface UserInterest extends Parcelable, Persistable {

    @Key
    @Generated
    int getId();


    String getInterest();

    void setInterest(String interest);


    @ManyToOne
    User getOwner();

    void setOwner(User person);
}
