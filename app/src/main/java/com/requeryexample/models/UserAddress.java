package com.requeryexample.models;


import android.os.Parcelable;

import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToOne;
import io.requery.Persistable;

/**
 * Created by Alex Fedorenko on 13.01.2017.
 */

@Entity
public interface UserAddress extends Parcelable, Persistable {

    @Key
    @Generated
    int getId();


    String getPhoneNumber();

    void setPhoneNumber(String phoneNumber);

    String getAddress();

    void setAddress(String address);

    @OneToOne(mappedBy = "userAddress")
    User getUser();
}
