package com.requeryexample.models;


import android.os.Parcelable;

import java.util.List;
import java.util.UUID;

import io.requery.CascadeAction;
import io.requery.Column;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.OneToOne;
import io.requery.Persistable;
import io.requery.query.MutableResult;

/**
 * Created by Alex Fedorenko on 13.01.2017.
 */

@Entity
public interface User extends Parcelable, Persistable {

    @Key
    @Generated
    int getId();

    String getFirstName();

    String getLastName();

    @ForeignKey
    @OneToOne
    UserAddress getUserAddress();

    @OneToMany(mappedBy = "owner", cascade = {CascadeAction.DELETE, CascadeAction.SAVE})
    MutableResult<UserInterest> getUserInterest();


    @Column(unique = true)
    UUID getUUID();

    @OneToMany(mappedBy = "owner", cascade = {CascadeAction.DELETE, CascadeAction.SAVE})
    List<UserInterest> getUserInterestList();


}
