package com.requeryexample.useredit;

import com.requeryexample.models.User;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public interface UserEditView  {

    void showProgress();

    void hideProgress();

    void onUserQueryCompleted(User user);

    void onUserSaveCompleted(User user);
}
