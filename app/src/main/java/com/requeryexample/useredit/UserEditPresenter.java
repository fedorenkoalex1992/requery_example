package com.requeryexample.useredit;

import android.util.Log;

import com.requeryexample.models.User;
import com.requeryexample.models.UserAddress;
import com.requeryexample.models.UserAddressEntity;
import com.requeryexample.models.UserEntity;
import com.requeryexample.models.UserInterest;
import com.requeryexample.models.UserInterestEntity;
import com.requeryexample.userslist.UsersView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class UserEditPresenter {

    private final UserEditView mUsersView;
    //Todo implement Dagger injection
    private ReactiveEntityStore<Persistable> mData;

    private UserEntity mCurrentUser;
    private int mUserId = -1;

    public UserEditPresenter(ReactiveEntityStore<Persistable> data, UserEditView view) {
        mUsersView = view;
        mData = data;
    }


    public void getUser(int id) {
        mUsersView.showProgress();
        mData.findByKey(UserEntity.class, id)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(userEntity -> {
                    if (userEntity != null) {
                        mCurrentUser = userEntity;
                        mUserId = id;
                        mUsersView.onUserQueryCompleted(mCurrentUser);
                        mUsersView.hideProgress();
                    }
                });
    }

    public void saveCurrentUser(String fName, String secName, String phone, String address, List<String> interests) {
        mUsersView.showProgress();

        if (mCurrentUser == null) {
            mCurrentUser = new UserEntity();
        }
        mCurrentUser.setFirstName(fName);
        mCurrentUser.setLastName(secName);

        UserAddress userAddress = mCurrentUser.getUserAddress();
        if (userAddress == null) {
            userAddress = new UserAddressEntity();
        }
        userAddress.setPhoneNumber(phone);
        userAddress.setAddress(address);
        mCurrentUser.setUserAddress(userAddress);

        mCurrentUser.getUserInterestList().clear();
        for (int i = 0; i < interests.size(); i++) {
            UserInterestEntity interestEntity = new UserInterestEntity();
            interestEntity.setInterest(interests.get(i));
            interestEntity.setOwner(mCurrentUser);
            mCurrentUser.getUserInterestList().add(interestEntity);
        }


        if (mUserId != -1) {
            mData.update(mCurrentUser)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(userEntity -> {
                mUsersView.hideProgress();
                mUsersView.onUserSaveCompleted(userEntity);
            });
        } else {
            mData.insert(mCurrentUser)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(userEntity -> {
                mUsersView.hideProgress();
                mUsersView.onUserSaveCompleted(userEntity);

            });
        }

    }

    public User getCurrentUser() {
        return mCurrentUser;
    }
}
