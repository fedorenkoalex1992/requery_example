package com.requeryexample.useredit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.requeryexample.App;
import com.requeryexample.BaseActivity;
import com.requeryexample.R;
import com.requeryexample.models.User;
import com.requeryexample.models.UserInterest;

import java.util.ArrayList;
import java.util.List;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class UserEditActivity extends BaseActivity implements UserEditView {


    private EditText mFName;
    private EditText mSecName;
    private EditText mAdrPhone;
    private EditText mAdrHome;
    private EditText mInterests;
    private ProgressBar mProgress;

    private UserEditPresenter mEditPresenter;

    public static final String USER_ID = "USER_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        renderView();

        ReactiveEntityStore<Persistable> data = ((App) getApplication()).getData();

        mEditPresenter = new UserEditPresenter(data, this);

        int userId = getIntent().getIntExtra(USER_ID, -1);
        if (userId != -1) {
            mEditPresenter.getUser(userId);
        }

    }

    @Override
    public void renderView() {
        super.renderView();
        setContentView(R.layout.activity_edit);
        mFName = (EditText) findViewById(R.id.edit_first_name);
        mSecName = (EditText) findViewById(R.id.edit_second_name);
        mAdrPhone = (EditText) findViewById(R.id.edit_phone_num);
        mAdrHome = (EditText) findViewById(R.id.edit_address);
        mInterests = (EditText) findViewById(R.id.edit_interests);
        mProgress = (ProgressBar) findViewById(R.id.user_edit_progress);
        (findViewById(R.id.edit_save)).setOnClickListener(v -> {
            List<String> interestsList = new ArrayList<>();
            String[] interestArray = mInterests.getText().toString().split("\n");
            for (int i = 0; i < interestArray.length; i++) {
                interestsList.add(interestArray[i]);
            }
            mEditPresenter.saveCurrentUser(mFName.getText().toString(), mSecName.getText().toString(), mAdrPhone.getText().toString(), mAdrHome.getText().toString(), interestsList);
        });
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onUserQueryCompleted(User user) {
        mFName.setText(user.getFirstName());
        mSecName.setText(user.getLastName());
        if (user.getUserAddress() != null) {
            mAdrPhone.setText(user.getUserAddress().getPhoneNumber());
            mAdrHome.setText(user.getUserAddress().getAddress());
        }
        List<UserInterest> interestList = user.getUserInterestList();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < interestList.size(); i++) {
            stringBuilder.append(interestList.get(i).getInterest());
            if (i < interestList.size()) {
                stringBuilder.append("\n");
            }
        }
        mInterests.setText(stringBuilder.toString());

    }

    @Override
    public void onUserSaveCompleted(User user) {
        UserEditActivity.this.finish();
    }
}
