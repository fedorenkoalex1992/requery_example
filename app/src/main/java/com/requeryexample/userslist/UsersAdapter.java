package com.requeryexample.userslist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.requeryexample.R;
import com.requeryexample.models.User;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by fedor on 06.12.2016.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.DeviceHolder> {

    private Context mContext;
    private AdapterListener mListener;
    private List<User> mList = new ArrayList<>();

    public void setListener(AdapterListener listener) {
        this.mListener = listener;
    }

    public UsersAdapter(Context context) {
        this.mContext = context;
    }

    public interface AdapterListener {
        void onAdapterClick(User item);

        void onAdapterLongClick(User item);
    }

    // 3
    public class DeviceHolder extends RecyclerView.ViewHolder {

        RelativeLayout container;
        TextView deviceName;

        public DeviceHolder(View itemView) {
            super(itemView);
            container = (RelativeLayout) itemView.findViewById(R.id.item_user_container);
            deviceName = (TextView) itemView.findViewById(R.id.item_user_text);
        }

        public void update(final User item) {
            deviceName.setText("" + item.getFirstName() + " - " + item.getLastName());

            container.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onAdapterClick(item);
                }
            });

            container.setOnLongClickListener(v -> {
                if (mListener != null) {
                    mListener.onAdapterLongClick(item);
                }
                return true;
            });
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(final DeviceHolder holder, final int position) {
        final User place = mList.get(position);
        holder.update(place);
    }

    public void update(List<User> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public User getItem(int postion) {
        return mList.get(postion);
    }
}
