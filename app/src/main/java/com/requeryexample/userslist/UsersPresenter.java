package com.requeryexample.userslist;

import com.requeryexample.models.User;
import com.requeryexample.models.UserAddress;
import com.requeryexample.models.UserAddressEntity;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class UsersPresenter {

    private final UsersView mUsersView;
    //Todo implement Dagger injection
    private ReactiveEntityStore<Persistable> mData;

    public UsersPresenter(ReactiveEntityStore<Persistable> data, UsersView view) {
        mUsersView = view;
        mData = data;
    }


    public void getUsersList() {
        mUsersView.showProgress();

        List<User> usersList = mData.select(User.class).get().toList();

        mUsersView.onUsersQueryCompleted(usersList);

        mUsersView.hideProgress();
    }


    public void remove(User user) {
        removeAttachedClasses(user);
        mData.delete(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    getUsersList();
                });
    }

    private void removeAttachedClasses(User user) {
        if (user.getUserAddress() != null) {
            mData.delete(user.getUserAddress()).subscribe();
        }
        mData.delete(user.getUserInterest()).subscribe();


    }
}
