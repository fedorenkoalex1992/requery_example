package com.requeryexample.userslist;

import com.requeryexample.models.User;

import java.util.List;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public interface UsersView {

    void showProgress();

    void hideProgress();

    void onUsersQueryCompleted(List<User> usersList);
}
