package com.requeryexample.userslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.requeryexample.App;
import com.requeryexample.BaseActivity;
import com.requeryexample.Dialoger;
import com.requeryexample.R;
import com.requeryexample.models.User;
import com.requeryexample.useredit.UserEditActivity;
import com.requeryexample.userinterests.UserInterestsActivity;

import java.util.List;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Alex Fedorenko on 16.01.2017.
 */

public class UsersActivity extends BaseActivity implements UsersView, UsersAdapter.AdapterListener {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private UsersAdapter mAdapter;
    private UsersPresenter usersPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        renderView();

        ReactiveEntityStore<Persistable> data = ((App) getApplication()).getData();

        usersPresenter = new UsersPresenter(data, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        usersPresenter.getUsersList();
    }

    @Override
    public void renderView() {
        super.renderView();
        setContentView(R.layout.activity_list);
        mRecyclerView = (RecyclerView) findViewById(R.id.users_list);
        mProgress = (ProgressBar) findViewById(R.id.users_progress);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        (findViewById(R.id.users_add)).setOnClickListener(v -> {
            Intent intent = new Intent(UsersActivity.this, UserEditActivity.class);
            intent.putExtra(UserEditActivity.USER_ID, -1);
            startActivity(intent);
        });


    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info_click:
                Intent intent = new Intent(UsersActivity.this, UserInterestsActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onUsersQueryCompleted(List<User> usersList) {
        mAdapter = new UsersAdapter(this);
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.update(usersList);
    }

    @Override
    public void onAdapterClick(User item) {
        int id = item.getId();
        Intent intent = new Intent(UsersActivity.this, UserEditActivity.class);
        intent.putExtra(UserEditActivity.USER_ID, id);
        startActivity(intent);
    }

    @Override
    public void onAdapterLongClick(User item) {
        Dialoger.getInstance(this).withMessage("Remove?").addCancelButton("Cancel").addButton("Yes", (dialog, which) -> {
            usersPresenter.remove(item);
        }).show();
    }
}
